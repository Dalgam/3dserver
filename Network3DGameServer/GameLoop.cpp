#define UNICODE
#define _UNICODE
#define _USE_MATH_DEFINES


#include "GameLoop.h"
#include <ws2tcpip.h>
#include <stdio.h>
#include <iostream>
#include <windows.h>
#include <gl/gl.h>
#include <gl/GLU.h>
#include <math.h>
#include "timer.h"
#include "vec3.h"
#include "network_protocol.h"
#include "Lists.h"
#include "win32_modern_opengl2.h"
#include "mat4.h"



#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_PORT "27020"
#define BUFSIZE 512

SOCKET ListenSocket;
addrinfo result;
ClientToServerMessage ctsmsg;
ServerToClientMessage stcmsg;
char recvbuff[512];
sockaddr_in clientaddrs[MAX_PLAYERS];
bool connected[MAX_PLAYERS];
vec3 startpos1 = Vec3(-2.f, 0.f, 0.f);
vec3 startpos2 = Vec3( 2.f, 0.f, 0.f);

GameState gamestate;
float bulletrotations[MAX_PLAYERS];

struct AABB
{
	vec3 max;
	vec3 min;
};

int GameLoop::Initialize()
{
	int iResult = InitializeNetwork(&result);
	for (int i = 0; i < MAX_PLAYERS; i++)
	{
		gamestate.players[i].ID = -1;
		gamestate.players[i].bulletPosition = Vec3(0, 0, 0);
		gamestate.players[i].bulletrotation = 0;
		gamestate.players[i].isBulletActive = false;
		gamestate.players[i].position = Vec3(0, 0, 0);
		gamestate.players[i].rotation = 0;
	}
	initTimerStuff();
	return 0;
}

int GameLoop::InitializeNetwork(addrinfo* result)
{
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	// Resolve the server address and port
	addrinfo hints = { 0 };
	hints.ai_family = AF_INET; //AF_UNSPEC = IPv4 or IPv6, AF_INET = IPv4, AF_INET6 = IPv6
	hints.ai_socktype = SOCK_DGRAM; // SOCK_STREAM = TCP, SOCK_DGRAM = UDP
	hints.ai_protocol = IPPROTO_UDP; // IPPROTO_TCP = TCP, IPPROTO_UDP = UDP
	hints.ai_flags = AI_PASSIVE;

	addrinfo* tempresult;
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &tempresult);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(tempresult->ai_family, tempresult->ai_socktype, tempresult->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(tempresult);
		WSACleanup();
		return 1;
	}

	// Setup the TCP listening socket
	iResult = bind(ListenSocket, tempresult->ai_addr, (int)tempresult->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(tempresult);
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}
	*result = *tempresult;

	u_long mode = 1;
	iResult = ioctlsocket(ListenSocket, FIONBIO, &mode);
	if (iResult == SOCKET_ERROR) {
		printf("non blocking error: %d\n", WSAGetLastError());
		freeaddrinfo(tempresult);
		closesocket(ListenSocket);
		WSACleanup();
	}
	return 0;
}

void checkcollision()
{
	AABB boxPlayer;
	boxPlayer.max = Vec3(0.25f, 0.25f, 0.6f);
	boxPlayer.min = Vec3(-0.25f, -0.25f, 0.0f);
	for (int i = 0; i < MAX_PLAYERS; i++)
	{
		for (int j = 0; j < MAX_PLAYERS; j++)
		{
			if (i != j)
			{
				if (gamestate.players[j].isBulletActive)
				{
					if (gamestate.players[j].bulletPosition.x > boxPlayer.min.x + gamestate.players[i].position.x && gamestate.players[j].bulletPosition.x < boxPlayer.max.x + gamestate.players[i].position.x &&
						gamestate.players[j].bulletPosition.y > boxPlayer.min.y + gamestate.players[i].position.y && gamestate.players[j].bulletPosition.y < boxPlayer.max.y + gamestate.players[i].position.y)
					{
						if (i == 0)
						{
							gamestate.players[i].position = startpos1;
							gamestate.players[i].rotation = M_PI;
						}
						if (i == 1)
						{
							gamestate.players[i].position = startpos2;
							gamestate.players[i].rotation = 0;

						}
						gamestate.players[j].isBulletActive = false;
					}
				}

			}
		}
	}
}

int GameLoop::Update()
{
	//from temporarily jolds the networkinfo from the connecting player
	sockaddr_in from;
	for (int i = 0; i < MAX_PLAYERS; i++)
	{
		int iResult = RecieveMessageFromClient(ListenSocket, (sockaddr*)&from, &ctsmsg, recvbuff);
		if (iResult == -1 && WSAGetLastError() == WSAECONNRESET)
		{
			for (int i = 0; i < MAX_PLAYERS; i++)
			{
				if (connected[i] && clientaddrs[i].sin_addr.S_un.S_addr == from.sin_addr.S_un.S_addr &&clientaddrs[i].sin_port == from.sin_port)
				{
					gamestate.players[i].ID = -1;
					connected[i] = false;
					
					printf("Player %d Disconnected : Lost Connection\n", i);

				}
			}
		}
		if (iResult > -1)
		{
			//printf("Bytes received: %d\n", iResult);

		}
		if (ctsmsg.type == MSG_FROMCLIENT_REQUEST_TO_JOIN)
		{
			gamestate.time = totalms;
			for (int i = 0; i < sizeof(gamestate.players) / sizeof(gamestate.players[0]); i++)
			{
				if (connected[i] == false)
				{
					//stores from in an array for later use to compare if the user is already connected and to send to
					//the respective player
					clientaddrs[i] = from;
					connected[i] = true;
				}
				if (gamestate.players[i].ID == -1 || gamestate.players[i].ID == ctsmsg.ID)
				{
					
					if (i == 0)
					{
						gamestate.players[i].position = startpos1;
						gamestate.players[i].rotation = M_PI;

					}
					if (i == 1)
					{
						gamestate.players[i].position = startpos2;
						gamestate.players[i].rotation = 0;
					}
					gamestate.players[i].ID = ctsmsg.ID;
					i = 55;
				}

			}
		}
		if (ctsmsg.type == MSG_FROMCLIENT_INPUT)
		{
			gamestate.time = totalms;
			for (int i = 0; i < sizeof(gamestate.players) / sizeof(gamestate.players[0]); i++)
			{
				if (gamestate.players[i].ID == ctsmsg.ID)
				{
					//update player position
					if (ctsmsg.input[LEFT])
					{
						gamestate.players[i].rotation += 0.01f;
					}
					if (ctsmsg.input[RIGHT])
					{
						gamestate.players[i].rotation -= 0.01f;

					}

					
					if (ctsmsg.input[UP])
					{
						gamestate.players[i].position = Vec3(gamestate.players[i].position.x + cos(gamestate.players[i].rotation) * 0.01f, gamestate.players[i].position.y + sin(gamestate.players[i].rotation) * 0.01f, gamestate.players[i].position.z);
					}
					if (ctsmsg.input[DOWN])
					{
						gamestate.players[i].position = Vec3(gamestate.players[i].position.x - cos(gamestate.players[i].rotation)* 0.01f, gamestate.players[i].position.y - sin(gamestate.players[i].rotation)* 0.01f, gamestate.players[i].position.z);
					}

					if (ctsmsg.input[SHOOT] && !gamestate.players[i].isBulletActive)
					{
						gamestate.players[i].isBulletActive = true;
						gamestate.players[i].bulletrotation = gamestate.players[i].rotation;
						gamestate.players[i].bulletPosition = gamestate.players[i].position;
					}
					if (gamestate.players[i].isBulletActive)
					{
						gamestate.players[i].bulletPosition = Vec3(gamestate.players[i].bulletPosition.x + cos(gamestate.players[i].bulletrotation)* 0.1f, gamestate.players[i].bulletPosition.y + sin(gamestate.players[i].bulletrotation)* 0.1f, gamestate.players[i].bulletPosition.z);

						if (gamestate.players[i].bulletPosition.x > 6.4f || gamestate.players[i].bulletPosition.x < -6.4f
							|| gamestate.players[i].bulletPosition.y > 6.4f || gamestate.players[i].bulletPosition.y < -6.4f)
						{
							gamestate.players[i].isBulletActive = false;
						}
					}
					i = 55;
				}
			}

		}
		if (ctsmsg.type == MSG_FROMCLIENT_DISCONNECT)
		{
			gamestate.time = totalms;
			for (int i = 0; i < sizeof(gamestate.players) / sizeof(gamestate.players[0]); i++)
			{
				if (gamestate.players[i].ID == ctsmsg.ID)
				{
					gamestate.players[i].ID = -1;
					connected[i] = false;
					printf("Player %d Disconnected : Quit\n", i);
					i = 55;
				}
			}
		}
	}

	
	checkcollision();

	stcmsg.gameState = gamestate;
	stcmsg.type = MSG_FROMSERVER_GAME_STATE;
	//Send gamestates to all clients
	for (int i = 0; i < MAX_PLAYERS; i++)
	{
		if (gamestate.players[i].ID != -1 && connected[i])
		{
			int iSendResult = SendMessageToClient(ListenSocket,(sockaddr*)&clientaddrs[i], &stcmsg);
			
			if (iSendResult > -1)
			{
				//printf("Bytes sent: %d\n", iSendResult);

			}
			if (iSendResult == -1 && WSAGetLastError() == WSAECONNRESET)
			{
				gamestate.players[i].ID = -1;
				connected[i] = false;
				printf("Player %d Disconnected : Lost Connection\n", i);
			}
			
		}
	}


	sleepyTime();
	gamestate.frameCount += 1;
	return 1;
}


int GameLoop::SendMessageToClient(SOCKET sockfd, sockaddr * server, ServerToClientMessage * msg)
{
	char sendbuf[BUFSIZE];

	SerializeForClient(msg, sendbuf);
	int len = sizeof(*server);
	int iResult = sendto(sockfd, sendbuf, BUFSIZE, 0, server, len);
	if (iResult == SOCKET_ERROR) {
		printf("sendto failed with error: %d\n", WSAGetLastError());
		//WSACleanup();
		//system("pause");
		return iResult;
	}
	return iResult;
}

int GameLoop::RecieveMessageFromClient(SOCKET sockfd, sockaddr * server, ClientToServerMessage * msg, char *recvbuf)
{

	int len = sizeof(*server);

	int iResult = recvfrom(sockfd, recvbuf, BUFSIZE, 0, server, &len);
	int temp = WSAGetLastError();
	//if()
	if (iResult == SOCKET_ERROR && WSAGetLastError() != WSAEWOULDBLOCK ) {
		printf("recvfrom failed with error: %d\n", WSAGetLastError());
		//WSACleanup();
		//system("pause");
		return iResult;
	}
	else if (iResult > 0)
	{
		DeserializeFromClient(recvbuf, msg);
	}

	return iResult;
}