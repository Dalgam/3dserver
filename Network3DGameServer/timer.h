#include <windows.h>
#include <stdio.h>

#pragma comment(lib, "Winmm.lib")


#define DESIRED_SECONDS_PER_FRAME (1.0/50.0) // 50 Hz

LARGE_INTEGER QPFrequency;
LARGE_INTEGER previousFrameCounter;
double timeAccumulator = 0.0;
double totalms = 0;

static inline LARGE_INTEGER getPerfCounter(){
	LARGE_INTEGER result;
	QueryPerformanceCounter(&result);
	return result;
}

static void initTimerStuff(){
	if(timeBeginPeriod(1) != TIMERR_NOERROR){
		printf("timeBeginPeriod == TIMERR_NOCANDO\n");
	}
	QueryPerformanceFrequency(&QPFrequency);
	previousFrameCounter = getPerfCounter();
}

static inline double getElapsedSeconds(LARGE_INTEGER start, LARGE_INTEGER end){
	double result = (double)(end.QuadPart - start.QuadPart) / (double)QPFrequency.QuadPart;
	return result;
}

void sleepyTime(){
	LARGE_INTEGER currentCounter = getPerfCounter();
	double dts = getElapsedSeconds(previousFrameCounter, currentCounter);
	totalms += dts;
	timeAccumulator += DESIRED_SECONDS_PER_FRAME;
	timeAccumulator -= dts;
	previousFrameCounter = currentCounter;

	//printf("delta time: %f\n", dts);

	// only sleep the frames our update function is faster than our desired frame time
	if(timeAccumulator > 0.0){
		int msToSleep = (int)(timeAccumulator * 1000.0);
		Sleep(msToSleep);
	}
}

/*
Usage:

1. Modify #define DESIRED_SECONDS_PER_FRAME to set desired frame rate

2. during init/setup, call:

		initTimerStuff();

3. last thing in game loop, call:

		sleepyTime();


Example:

int main(){
	initTimerStuff();

	for(int i = 0; i < 10; i++){
		sleepyTime();
	}
}

*/