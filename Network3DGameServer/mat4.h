/*

// usage in draw()

// create frustum
mat4 m = createFrustumMatrix(-1,1,-1,1,3,3000);

// move camera
translate(&m, 0,0,-10);

mat4 matrix1 = m;
translate(&matrix1, 0,0,3); // translate 0x, 0y, 3z
rotate(&matrix1, M_PI, 0,0,1); // rotate 180° around z-axis
scale(&matrix1, 2,1,1); // set scale x=2, y=1, z=1
// send down matrix1 to a uniform mat4 in vertex shader
// ...
// draw mesh
// ...

mat4 matrix2 = m;
translate(&matrix2, 3,0,0);
rotate(&matrix2, 0.2, 1,0,0);
scale(&matrix2, 0.4,0.4,0.4);
// send down matrix2 to a uniform mat4 in vertex shader
// ...
// draw mesh
// ...

*/


struct mat4 {
	float m[16];
};

static mat4 identity(){
	mat4 result = {0};
	result.m[0]=result.m[5]=result.m[10]=result.m[15]=1.f;
	return result;
}

static mat4 createFrustumMatrix(float left, float right, float bottom, float top, float nearVal, float farVal){
	float A = (right+left) / (right-left);
	float B = (top+bottom) / (top-bottom);
	float C = -(farVal+nearVal) / (farVal-nearVal);
	float D = -(2*farVal*nearVal) / (farVal-nearVal);

	mat4 result;
	result.m[0] = (2*nearVal) / (right-left);
	result.m[1] = 0.f;
	result.m[2] = 0.f;
	result.m[3] = 0.f;

	result.m[4] = 0.f;
	result.m[5] = (2*nearVal) / (top-bottom);
	result.m[6] = 0.f;
	result.m[7] = 0.f;

	result.m[8] = A;
	result.m[9] = B;
	result.m[10] = C;
	result.m[11] = -1.f;

	result.m[12] = 0.f;
	result.m[13] = 0.f;
	result.m[14] = D;
	result.m[15] = 0.f;
	return result;
}

/*              OpenGL / GLSL. [12],[13],[14] = tx, ty, tz (translation)
 0  1  2  3     0  4  8 12
 4  5  6  7     1  5  9 13
 8  9 10 11     2  6 10 14
12 13 14 15     3  7 11 15
*/
static mat4 multiply(mat4 *a, mat4 *b){
	mat4 result;

	// column 1
	result.m[0] = a->m[0]*b->m[0] + a->m[4]*b->m[1] + a->m[8]*b->m[2] + a->m[12]*b->m[3];
	result.m[1] = a->m[1]*b->m[0] + a->m[5]*b->m[1] + a->m[9]*b->m[2] + a->m[13]*b->m[3];
	result.m[2] = a->m[2]*b->m[0] + a->m[6]*b->m[1] + a->m[10]*b->m[2] + a->m[14]*b->m[3];
	result.m[3] = a->m[3]*b->m[0] + a->m[7]*b->m[1] + a->m[11]*b->m[2] + a->m[15]*b->m[3];

	// column 2
	result.m[4] = a->m[0]*b->m[4] + a->m[4]*b->m[5] + a->m[8]*b->m[6] + a->m[12]*b->m[7];
	result.m[5] = a->m[1]*b->m[4] + a->m[5]*b->m[5] + a->m[9]*b->m[6] + a->m[13]*b->m[7];
	result.m[6] = a->m[2]*b->m[4] + a->m[6]*b->m[5] + a->m[10]*b->m[6] + a->m[14]*b->m[7];
	result.m[7] = a->m[3]*b->m[4] + a->m[7]*b->m[5] + a->m[11]*b->m[6] + a->m[15]*b->m[7];

	// column 3
	result.m[8] = a->m[0]*b->m[8] + a->m[4]*b->m[9] + a->m[8]*b->m[10] + a->m[12]*b->m[11];
	result.m[9] = a->m[1]*b->m[8] + a->m[5]*b->m[9] + a->m[9]*b->m[10] + a->m[13]*b->m[11];
	result.m[10] = a->m[2]*b->m[8] + a->m[6]*b->m[9] + a->m[10]*b->m[10] + a->m[14]*b->m[11];
	result.m[11] = a->m[3]*b->m[8] + a->m[7]*b->m[9] + a->m[11]*b->m[10] + a->m[15]*b->m[11];

	// column 4
	result.m[12] = a->m[0]*b->m[12] + a->m[4]*b->m[13] + a->m[8]*b->m[14] + a->m[12]*b->m[15];
	result.m[13] = a->m[1]*b->m[12] + a->m[5]*b->m[13] + a->m[9]*b->m[14] + a->m[13]*b->m[15];
	result.m[14] = a->m[2]*b->m[12] + a->m[6]*b->m[13] + a->m[10]*b->m[14] + a->m[14]*b->m[15];
	result.m[15] = a->m[3]*b->m[12] + a->m[7]*b->m[13] + a->m[11]*b->m[14] + a->m[15]*b->m[15];

	return result;
}

static void translate(mat4 * m, float x, float y, float z){
	mat4 t;
	t.m[0] = 1;   t.m[4] = 0;  t.m[8] = 0;  t.m[12] = x;
	t.m[1] = 0;   t.m[5] = 1;  t.m[9] = 0;  t.m[13] = y;
	t.m[2] = 0;   t.m[6] = 0;  t.m[10] = 1;  t.m[14] = z;
	t.m[3] = 0;   t.m[7] = 0;  t.m[11] = 0;  t.m[15] = 1;
	*m = multiply(m, &t);
}

static void rotate(mat4 * m, float angleRadians, float x, float y, float z){
	mat4 r;
	vec3 v = Vec3(x,y,z);
	v = normalize(v);
	x = v.x;
	y = v.y;
	z = v.z;
	float c = cos(angleRadians);
	float s = sin(angleRadians);
	float cN = 1.f - c;
	r.m[0] = x*x*cN+c;    r.m[4] = y*x*cN-z*s;  r.m[8]  = x*z*cN+y*s;  r.m[12] = 0;
	r.m[1] = x*y*cN+z*s;  r.m[5] = y*y*cN+c;    r.m[9]  = y*z*cN-x*s;  r.m[13] = 0;
	r.m[2] = x*z*cN-y*s;  r.m[6] = y*z*cN+x*s;  r.m[10] = z*z*cN+c;    r.m[14] = 0;
	r.m[3] = 0;           r.m[7] = 0;           r.m[11] = 0;           r.m[15] = 1;
	*m = multiply(m, &r);
}

static void scale(mat4 * m, float x, float y, float z){
	mat4 s;
	s.m[0] = x;   s.m[4] = 0;  s.m[8] = 0;  s.m[12] = 0;
	s.m[1] = 0;   s.m[5] = y;  s.m[9] = 0;  s.m[13] = 0;
	s.m[2] = 0;   s.m[6] = 0;  s.m[10] = z;  s.m[14] = 0;
	s.m[3] = 0;   s.m[7] = 0;  s.m[11] = 0;  s.m[15] = 1;
	*m = multiply(m, &s);
}