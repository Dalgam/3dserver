#pragma once

#include <winsock2.h>

struct ClientToServerMessage;
struct ServerToClientMessage;

class GameLoop
{
public:
	int Initialize();
	int Update();

private:
	
	int InitializeNetwork(addrinfo* result);

	int SendMessageToClient(SOCKET sockfd, sockaddr * server, ServerToClientMessage * msg);
	int RecieveMessageFromClient(SOCKET sockfd, sockaddr * server, ClientToServerMessage * msg, char *recvbuf);
	addrinfo result;
};