
#include "GameLoop.h";

int running = 1;

int main(int argc, char **argv) {
	GameLoop *Loop = new GameLoop();
	Loop->Initialize();
	while (running > 0)
	{
		running = Loop->Update();
	}
	delete(Loop);
	Loop = nullptr;
}

