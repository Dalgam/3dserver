union vec3 {
	struct { float x, y, z; };
	struct { float r, g, b; };
};

inline vec3 Vec3(float a = 0) {
	vec3 v;
	v.x = v.y = v.z = a;
	return v;
}
inline vec3 Vec3(float x, float y, float z) {
	vec3 v;
	v.x = x;
	v.y = y;
	v.z = z;
	return v;
}

inline vec3 operator *   (vec3 a, float b) { return Vec3(a.x*b, a.y*b, a.z*b); }
inline vec3 operator *   (float b, vec3 a) { return a * b; }
inline vec3 operator /   (vec3 a, float b) { return a * (1.f / b); }
inline vec3 operator *=  (vec3 &a, float b) { a = a * b; return a; }
inline vec3 operator -   (vec3 a) { return Vec3(-a.x, -a.y, -a.z); }
inline vec3 operator -   (vec3 a, vec3 b) { return Vec3(a.x - b.x, a.y - b.y, a.z - b.z); }
inline vec3 operator -=  (vec3 &a, vec3 b) { a = a - b; return a; }
inline vec3 operator +   (vec3 a, vec3 b) { return Vec3(a.x + b.x, a.y + b.y, a.z + b.z); }
inline vec3 operator +=  (vec3 &a, vec3 b) { a = a + b; return a; }

inline float dot(vec3 a, vec3 b) {
	return a.x*b.x + a.y*b.y + a.z*b.z;
}
inline vec3 cross(vec3 a, vec3 b) {
	return Vec3(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
}
inline float length(vec3 v) {
	return sqrt(dot(v, v));
}
inline vec3 normalize(vec3 v) {
	return v / length(v);
}
inline vec3 lerpvec3(vec3 start, vec3 end, float time)
{
	vec3 result = Vec3(start.x + time *(end.x - start.x), start.y + time *(end.y - start.y), start.z + time *(end.z - start.z));
	return result;
}

inline float lerp(float start, float end, float time)
{
	return start + time *(end - start);
}