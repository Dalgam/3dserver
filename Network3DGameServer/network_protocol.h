// this is a simple protocol for sending messages between game and server
// include this in client and server



#define MSG_FROMCLIENT_REQUEST_TO_JOIN 1
#define MSG_FROMCLIENT_DISCONNECT 2
#define MSG_FROMCLIENT_INPUT 3

#define MSG_FROMSERVER_GAME_STATE 1
#define MSG_FROMSERVER_REQUEST_DENIED 2
#define MSG_FROMSERVER_DISCONNECTED 3

#define SERVER_PORT "27020"

#define MAX_PLAYERS 2

enum direction {
	UP,
	DOWN,
	LEFT,
	RIGHT,
	SHOOT
};

struct ClientToServerMessage {
	char type;
	bool input[5]; //up, down, left, right, shoot
	char ID;
};

struct Player {
	char ID; // 0+ for valid player ID, -1 for empty slot
	vec3 position;
	vec3 bulletPosition;
	float rotation;
	float bulletrotation;
	char isBulletActive; //1 = true, -1 = false;
};

struct GameState {
	// frameCount is to make sure we don't overwrite client state
	// with older server game state when messages arrive out of order
	unsigned int frameCount;
	double time;
	Player players[MAX_PLAYERS];
};

struct ServerToClientMessage {
	char type;
	char reason;
	GameState gameState;
};


//add: position, 

void SerializeForClient(ServerToClientMessage* msgPacket, char *data)
{
	unsigned int *p = (unsigned int*)data;
	*p = msgPacket->gameState.frameCount; p++;
	/*for (int l = 0; l < MAX_PLAYERS; l++)
	{
		*p = msgPacket->gameState.players[l].rotation; p++;
	}*/
	
	char *q = (char*)p;
	*q = msgPacket->type;       q++;
	*q = msgPacket->reason;   q++;
	for (int l = 0; l < MAX_PLAYERS; l++)
	{
		*q = msgPacket->gameState.players[l].ID; q++;
	}
	for (int l = 0; l < MAX_PLAYERS; l++)
	{
		*q = msgPacket->gameState.players[l].isBulletActive; q++;
	}
	float *d = (float*)q;
	for (int l = 0; l < MAX_PLAYERS; l++)
	{

		*d = msgPacket->gameState.players[l].position.x; d++;
		*d = msgPacket->gameState.players[l].position.y; d++;
		*d = msgPacket->gameState.players[l].position.z; d++;
		*d = msgPacket->gameState.players[l].bulletPosition.x; d++;
		*d = msgPacket->gameState.players[l].bulletPosition.y; d++;
		*d = msgPacket->gameState.players[l].bulletPosition.z; d++;
		*d = msgPacket->gameState.players[l].rotation; d++;
		*d = msgPacket->gameState.players[l].bulletrotation; d++;
	}
	double *k = (double*)d;
	*k = msgPacket->gameState.time; k++;


}

void DeserializeFromServer(char *data, ServerToClientMessage* msgPacket)
{
	unsigned int *p = (unsigned int*)data;
	msgPacket->gameState.frameCount = *p; p++;
	/*for (int l = 0; l < MAX_PLAYERS; l++)
	{
		msgPacket->gameState.players[l].rotation = *p; p++;
	}*/
	char *q = (char*)p;
	msgPacket->type = *q;       q++;
	msgPacket->reason = *q;   q++;
	for (int l = 0; l < MAX_PLAYERS; l++)
	{
		msgPacket->gameState.players[l].ID = *q; q++;
	}

	for (int l = 0; l < MAX_PLAYERS; l++)
	{
		msgPacket->gameState.players[l].isBulletActive = *q; q++;
	}
	float *d = (float*)q;
	for (int l = 0; l < MAX_PLAYERS; l++)
	{

		msgPacket->gameState.players[l].position.x = *d; d++;
		msgPacket->gameState.players[l].position.y = *d; d++;
		msgPacket->gameState.players[l].position.z = *d; d++;
		msgPacket->gameState.players[l].bulletPosition.x = *d; d++;
		msgPacket->gameState.players[l].bulletPosition.y = *d; d++;
		msgPacket->gameState.players[l].bulletPosition.z = *d; d++;
		msgPacket->gameState.players[l].rotation = *d; d++;
		msgPacket->gameState.players[l].bulletrotation = *d; d++;
	}
	double *k = (double*)d;
	msgPacket->gameState.time = *k; k++;
}

void DeserializeFromClient(char *data, ClientToServerMessage* msgPacket)
{
	char *q = (char*)data;
	msgPacket->type = *q;       q++;
	msgPacket->ID = *q;		 q++;
	bool *p = (bool*)q;
	for (int l = 0; l < sizeof(msgPacket->input) / sizeof(msgPacket->input[0]); l++)
	{
		msgPacket->input[l] = *p; p++;
	}
}

void SerializeForServer(ClientToServerMessage* msgPacket, char *data)
{
	char *q = (char*)data;
	*q = msgPacket->type;       q++;
	*q = msgPacket->ID;		q++;
	bool *p = (bool*)q;
	for (int l = 0; l < sizeof(msgPacket->input) / sizeof(msgPacket->input[0]); l++)
	{
		*p = msgPacket->input[l]; p++;
	}
}





/*

struct ClientToServerMessage {
char type;
unsigned int frameCount;
char leftButtonIsDown;
char rightButtonIsDown;
char upButtonIsDown;
char downButtonIsDown;
};

struct Player {
char ID; // 0+ for valid player ID, -1 for empty slot
float x;
float y;
float angle;
};

struct ServerToClientMessage {
char type;
union {
char requestDeniedReason;
struct {
unsigned int frameCount;
Player players[MAX_PLAYERS];
// ...
} gameState;
};
};

*/